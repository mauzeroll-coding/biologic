# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 12:57:35 2023

@author: Elliot
"""

import os
import matplotlib.pyplot as plt
import pandas as pd

# # Folder path containing the .mpt files ** windows **
folder_path = 'C:\\Users\\Elliot\\SynologyDrive\\Research - Elliot Howell\\Durbis CV Measurements\\TATPA'

# Get a list of files ending with '.mpt' in the folder ** WINDOWS **
txt_files = [f for f in os.listdir(folder_path) if f.endswith('.txt')]

# Process each .mpt file
for file_name in txt_files:
    file_path = os.path.join(folder_path, file_name)

    # Read the EC-Lab CV data
    cv_data = pd.read_csv(file_path, encoding = 'unicode_escape', skiprows = 53, sep = '\t', header = 0)
    
    # Keeping track of processed cycles
    processed_cycles = set()
    
    # Plot each cycle in a separate figure
    for cycle_number in cv_data['cycle number']:
        
        
        # Check if the cycle number has been processed before
        if cycle_number in processed_cycles:
            continue
        
        # Mark the cycle as processed
        processed_cycles.add(cycle_number)
        
        # Testing to make sure we are extracting correct data
        # n+=1
        
        # print(n)
        cycle_data = cv_data[cv_data['cycle number'] == cycle_number]
        
        # if cycle_number == 3:
        #     print(cycle_data)
        
        # Extract potential and current for the cycle
        potential = cycle_data['Ewe/V']
        current = 1000*cycle_data['<I>/mA']

        # Plot the data
        plt.figure()
        plt.plot(potential, current)

        # Customize the plot
        plt.title(f'{file_name} - Cycle {cycle_number}')
        plt.xlabel('Potential (V vs. SCE)')
        plt.ylabel('Current (uA)')
        plt.grid(True)

        # Show the plot for the current cycle
        plt.show()
        